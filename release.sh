#!/bin/bash
#===================================================================================
#
# FILE: release.sh
#
# USAGE: release.sh
#
# DESCRIPTION: Deploy ELASTIC dataClay into DockerHub
#
# OPTIONS: see function ’usage’ below
# REQUIREMENTS: ---
# BUGS: ---
# NOTES: ---
# AUTHOR: dgasull@bsc.es
# COMPANY: Barcelona Supercomputing Center (BSC)
# VERSION: 1.0
#===================================================================================


function prepare_buildx {

  REQUIRED_DOCKER_VERSION=19

  ############################# Prepare docker builder #############################
  printf "Checking if docker version >= $REQUIRED_DOCKER_VERSION..."
  version=$(docker version --format '{{.Server.Version}}')
  if [[ "$version" < "$REQUIRED_DOCKER_VERSION" ]]; then
    echo "ERROR: Docker version is less than $REQUIRED_DOCKER_VERSION"
    exit 1
  fi
  printf "OK\n"

  # prepare architectures
  docker run --rm --privileged docker/binfmt:a7996909642ee92942dcd6cff44b9b95f08dad64
  #docker run --rm --privileged multiarch/qemu-user-static --reset -p yes
  docker run --rm -t arm64v8/ubuntu uname -m

  DOCKER_BUILDER=$(docker buildx create)
  docker buildx use $DOCKER_BUILDER

  echo "Checking buildx with available platforms to simulate..."
  docker buildx inspect --bootstrap
  BUILDER_PLATFORMS=$(docker buildx inspect --bootstrap | grep Platforms | awk -F":" '{print $2}')
  IFS=',' read -ra BUILDER_PLATFORMS_ARRAY <<<"$BUILDER_PLATFORMS"
  IFS=',' read -ra SUPPORTED_PLATFORMS_ARRAY <<<"$PLATFORMS"
  echo "Builder created with platforms: ${BUILDER_PLATFORMS_ARRAY[@]}"

  #Print the split string
  for i in "${SUPPORTED_PLATFORMS_ARRAY[@]}"; do
    FOUND=false
    SUP_PLATFORM=$(echo $i | sed 's/ *$//g') #remove spaces
    printf "Checking if platform $i can be simulated by buildx..."
    for j in "${BUILDER_PLATFORMS_ARRAY[@]}"; do
      B_PLATFORM=$(echo $j | sed 's/ *$//g') #remove spaces
      if [ "$SUP_PLATFORM" == "$B_PLATFORM" ]; then
        FOUND=true
        break
      fi
    done
    if [ "$FOUND" = false ]; then
      echo "ERROR: missing support for $i in buildx builder."
      echo " Check https://github.com/multiarch/qemu-user-static for more information on how to simulate architectures"
      return 1
    fi
    printf "OK\n"

  done

}

#-----------------------------------------------------------------------
# MAIN
#-----------------------------------------------------------------------
echo "Welcome to ELASTIC-dataClay deployment script, please which version do you want to publish?"
read VERSION
echo "Going to publish:"
echo " -- elasticeuh2020/kafka:${VERSION} "
read -r -p "Are you sure? [y/N] " response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])$ ]]; then

  SECONDS=0

  export PLATFORMS=linux/amd64,linux/arm64
  prepare_buildx

  ############################# Push into DockerHub #############################

  docker login -u=elasticeuh2020

  echo " ===== Building elasticeuh2020/kafka:${VERSION} ====="
  docker buildx build --rm -f kafka.Dockerfile -t elasticeuh2020/kafka:$VERSION \
    --platform $PLATFORMS --push .

  docker logout
  docker buildx rm $DOCKER_BUILDER

  #######################################################################################

  duration=$SECONDS
  echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
  echo "Deployment FINISHED! "

  echo "  ==  Tagging new release in Git"
  #git tag -a ${VERSION} -m "tag $VERSION for components: elasticeuh2020/dataclay-initializer:$VERSION and elasticeuh2020/kafka:$VERSION "
  #git push origin ${VERSION}

  echo "  ==  Everything seems to be ok! Bye"

else
  echo "Aborting"
fi
