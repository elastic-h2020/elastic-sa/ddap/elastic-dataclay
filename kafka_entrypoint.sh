#!/usr/bin/env sh

# piping to sed prefixes the logs with the corresponding application name
run_zookeeper() {
    bin/zookeeper-server-start.sh config/zookeeper.properties | sed "s/^/[zookeeper] /"
}

wait_zookeeper() {
    echo "Waiting for zookeeper"
    while ! nc -z localhost 2181; do
        sleep 0.1
    done
}

run_kafka() {
    echo -e "\nadvertised.host.name=${KAFKA_HOST}" >> config/server.properties
    echo -e "\nadvertised.port=${KAFKA_PORT}" >> config/server.properties
    bin/kafka-server-start.sh config/server.properties | sed "s/^/[kafka-broker] /"
}


wait_kafka() {
    echo "Waiting for kafka"
    while ! nc -z ${KAFKA_HOST} ${KAFKA_PORT}; do
        echo "Waiting for kafka"
        sleep 10
    done
    echo "==> Conneceted to kafka!"
}

create_topics() {
    bin/kafka-topics.sh --create --bootstrap-server ${KAFKA_HOST}:${KAFKA_PORT} --replication-factor 1 --partitions 1 --topic dataclay
}

COMMAND=$1
KAFKA_HOST=${KAFKA_HOST:-localhost}
KAFKA_PORT=${KAFKA_PORT:-9092}
if [ "$COMMAND" = "consume" ]; then
  wait_kafka
  bin/kafka-console-consumer.sh --bootstrap-server ${KAFKA_HOST}:${KAFKA_PORT} --topic dataclay --from-beginning | sed "s/^/[kafka-consumer] /"
elif [ "$COMMAND" = "service-start" ]; then
  # run both in parallel but wait for zookeeper to startup
  { run_zookeeper & (create_topics) & (wait_zookeeper && run_kafka); }
else
  echo "ERROR: no command provided"
  exit 1
fi