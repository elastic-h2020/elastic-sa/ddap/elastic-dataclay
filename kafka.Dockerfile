FROM alpine:3 as build

ARG kafka_version=2.6.1
ARG scala_version=2.13
ENV KAFKA_VERSION=$kafka_version
ENV SCALA_VERSION=$scala_version

RUN apk add --update --no-cache curl jq wget tar
RUN FILENAME="kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz" \
            && url=$(curl --stderr /dev/null "https://www.apache.org/dyn/closer.cgi?path=/kafka/${KAFKA_VERSION}/${FILENAME}&as_json=1" | jq -r '"\(.preferred)\(.path_info)"') \
            && echo "Downloading Kafka from $url" \
            && wget "${url}" -O /tmp/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz \
            && tar -xf /tmp/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz -C "/tmp"

# Kafka and Zookeeper
FROM openjdk:8-jre-alpine

ARG kafka_version=2.6.1
ARG scala_version=2.13

ENV KAFKA_VERSION=$kafka_version
ENV SCALA_VERSION=$scala_version
ENV KAFKA_HOME=/opt/kafka
ENV PATH=${PATH}:${KAFKA_HOME}/bin
RUN mkdir -p ${KAFKA_HOME}

RUN apk add --no-cache bash

COPY --from=build "/tmp/kafka_${SCALA_VERSION}-${KAFKA_VERSION}" "/opt/kafka"

COPY ./kafka_entrypoint.sh $KAFKA_HOME/entrypoint.sh
WORKDIR $KAFKA_HOME
# 2181 is zookeeper, 9092 is kafka
EXPOSE 2181 9092

ENTRYPOINT ["./entrypoint.sh"]
CMD ["service-start"]
