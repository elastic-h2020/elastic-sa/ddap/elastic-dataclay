#!/bin/bash
#===================================================================================
#
# FILE: build.sh
#
# USAGE: build.sh
#
# DESCRIPTION: Build ELASTIC dataClay images
#
# OPTIONS: --
# REQUIREMENTS: ---
# BUGS: ---
# NOTES: ---
# AUTHOR: dgasull@bsc.es
# COMPANY: Barcelona Supercomputing Center (BSC)
# VERSION: 1.0
#===================================================================================
echo "Welcome to ELASTIC-dataClay build script, please which version do you want to build?"
VERSION=1.6
echo "Going to build: elasticeuh2020/kafka:${VERSION} "

SECONDS=0
echo " ===== Building elasticeuh2020/kafka:${VERSION} ====="
docker build --rm -f kafka.Dockerfile -t elasticeuh2020/kafka:$VERSION .

#######################################################################################
docker images | grep elasticeuh2020
duration=$SECONDS
echo "$(($duration / 60)) minutes and $(($duration % 60)) seconds elapsed."
echo "ELASTIC dataClay deployment FINISHED! "

